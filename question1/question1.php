<?php

function generateWord ($hour) {
    $f = new \NumberFormatter("en", NumberFormatter::SPELLOUT);
    return $f->format($hour);
}

function timeInWords($h, $m) {
    // if minute is 0
    if ($m == 0) {
        $hourInWord = generateWord($h);
        echo $hourInWord." o' clock";
        return;
    }

    // if minute is 30
    if ($m == 30) {
        $hourInWord = generateWord($h);
        echo "half past ".$hourInWord;
        return;
    }

    // if minute less than 30
    if ($m < 30) {
        $hourInWord = generateWord($h);
        if ($m == 15) {
            echo "quarter past ".$hourInWord;
            return;
        }
        $minuteInWord = generateWord($m);
        if ($m == 1) {
            echo $minuteInWord." minute past ".$hourInWord;
            return;
        }
        echo $minuteInWord." minutes past ".$hourInWord;
        return;
    }

    // if minute more than 30
    $newM = 60 - $m;
    $hourInWord = generateWord($h+1);
    if ($newM == 15) {
        echo "quarter to ".$hourInWord;
        return;
    }
    $minuteInWord = generateWord($newM);
    echo $minuteInWord." minutes to ".$hourInWord;
    return;
}

$inputHour = fopen("php://stdin","r");
$hour = trim(fgets($inputHour));
$inputMinute = fopen("php://stdin","r");
$minute = trim(fgets($inputHour));
$status = true;
if ($hour > 12 || $hour < 1) {
    echo "Hour must be between 1 and 12\n";
    $status = false;
}
if ($minute > 60 || $minute < 0) {
    echo "Minute must be between 0 and 60";
    $status = false;
}
if ($status === true) {
    timeInWords($hour, $minute);
}
