<?php

function generateArray($queries, $length) {
    $res = [];
    $numberQueries = count($queries);
    for ($i = 0; $i < $numberQueries; $i++) {
        $res[$i] = array_fill(0, $length, 0);
    }
    return $res;
}

function addIntoArray($index, $a, $b, $k, $resArray, $max = 0) {
    $lenArray = count($resArray);
    for ($i = $index; $i<$lenArray; $i++) {
        $changeArray = $resArray[$i];
        for ($j = $a-1; $j < $b;$j++) {
            $changeArray[$j] = $changeArray[$j] + $k;
            if ($changeArray[$j] > $max) {
                $max = $changeArray[$j];
            }
        }
        $resArray[$i] = $changeArray;
    }
    return [$resArray, $max];
}

function arrayManipulation($queries, $n, $m) {
    $resArray = generateArray($queries, $n);
    $max = 0;
    for ($i=0; $i < $m; $i++) {
        $a = $queries[$i][0];
        $b = $queries[$i][1];
        $k = $queries[$i][2];
        $res = addIntoArray($i, $a, $b, $k, $resArray, $max);
        $resArray = $res[0];
        $max = $res[1];
    }
    return $max;
}

function input() {
    echo "input: \n";
    $inputLineOne = fopen("php://stdin","r");
    $lineOne = trim(fgets($inputLineOne));
    $lineOneSplit = explode(" ", $lineOne);
    if (count($lineOneSplit) !== 2) {
        echo "incorrect input";
        return;
    }

    $n = $lineOneSplit[0];
    $m = $lineOneSplit[1];
    if (!is_numeric($n) || !is_numeric($m)) {
        echo "incorrect input";
        return;
    }
    $queries = [];

    for ($i = 0; $i < $m; $i++) {
        $inputQuery = fopen("php://stdin","r");
        $query = trim(fgets($inputQuery));
        $queryArray = explode(" ", $query);
        if (count($queryArray) !== 3) {
            echo "incorrect input";
            return;
        }
        $a = $queryArray[0];
        $b = $queryArray[1];
        $k = $queryArray[2];
        
        if($a < 1 || $b < $a || $b > $n || !is_numeric($a) || !is_numeric($b) || !is_numeric($k)) {
            echo "incorrect input";
            return;
        }
        array_push($queries, $queryArray);
    }

    echo "output: \n";
    echo arrayManipulation($queries, $n, $m);

}

$queries = input();