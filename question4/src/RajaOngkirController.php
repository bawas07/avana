<?php
namespace App;

use App\CurlController;

class RajaOngkirController
{
    function __construct() {
        $this->curl = new CurlController();
        $this->provinceUrl = 'https://api.rajaongkir.com/starter/province';
        $this->cityUrl = 'https://api.rajaongkir.com/starter/city';
        $this->costUrl = 'https://api.rajaongkir.com/starter/cost';
        $this->headers = [
            'key: '.getenv('RAJA_ONGKIR_KEY')
        ];
      }

    public function listProvince() {
        $res = $this->curl->CallAPI('GET', $this->provinceUrl, $this->headers);
        return $res['rajaongkir']['results'];
    }

    public function listCity() {
        $res = $this->curl->CallAPI('GET', $this->cityUrl, $this->headers);
        return $res['rajaongkir']['results'];
    }

    public function getProvince($id) {
        $url = $this->provinceUrl."?id=".$id;
        $res = $this->curl->CallAPI('GET', $url, $this->headers);
        return $res['rajaongkir']['results'];
    }

    public function getCity($id, $provinceId) {
        $url = $this->cityUrl."?id=".$id."&province=".$provinceId;
        $res = $this->curl->CallAPI('GET', $url, $this->headers);
        return $res['rajaongkir']['results'];
    }

    public function getCost($originID, $destID, $weight, $courier) {
        $body = [
            "origin" => $originID,
            "destination" => $destID,
            "weight" => $weight,
            "courier" => $courier
        ];
        $res = $this->curl->CallAPI('POST', $this->costUrl, $this->headers, $body);
        $from = $res['rajaongkir']["origin_details"];
        $to = $res['rajaongkir']["destination_details"];
        $result = [
            "from" => "$from[city_name] - $from[province]",
            "to" => "$to[city_name] - $to[province]",
            "courier" => $res['rajaongkir']['results'][0]['name'],
            "costs" => $res['rajaongkir']['results'][0]['costs']
        ];
        return $result;
    }
}