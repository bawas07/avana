<?php
require 'vendor/autoload.php';

use Symfony\Component\Dotenv\Dotenv;
use App\RajaOngkirController;

// Load .env file
if (file_exists ( __DIR__.'/.env' )) {
    $dotenv = new Dotenv();
    $dotenv->load(__DIR__.'/.env');
} else {
    echo "cannot find env files \n";
}

$rajaongkir = new RajaOngkirController;
echo json_encode($rajaongkir->listProvince(), JSON_PRETTY_PRINT);
echo json_encode($rajaongkir->listCity(), JSON_PRETTY_PRINT);
echo json_encode($rajaongkir->getProvince(12), JSON_PRETTY_PRINT);
echo json_encode($rajaongkir->getCity(39,5), JSON_PRETTY_PRINT);
echo json_encode($rajaongkir->getCost(501, 114, 1700, "jne"), JSON_PRETTY_PRINT);