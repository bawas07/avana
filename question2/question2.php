<?php

function findMaxLength($queries) {
    $max = 0;
    foreach ($queries as $query) {
        $maxSubString = $query[1] - $query[0] + 1;
        if ($max < $maxSubString) {
            $max = $maxSubString;
        }
    }
    return $max;
}

function countSubString($arrString, $start, $end) {
    $res = [];
    $uniqueRes = [];
    $result = 0;
    $max = $end - $start + 1;
    for ($i = $start; $i < $end+1; $i++) {
        for ($j=1;$j<=$max;$j++) {
            if ($j+$i <= $end+1) {
                if (!array_key_exists($j, $res)) {
                    $res[$j] = [];
                }
                array_push($res[$j], implode('', array_slice($arrString, $i, $j)));
            }
        }
    }
    foreach ($res as $value) {
        $unique = array_unique($value);
        $result = $result + count($unique);
    }
    return $result;
}

function uniquesubString ($string, $queries) {
    $arrayString = str_split($string);
    $res = [];
    foreach ($queries as $query) {
        echo countSubString($arrayString, $query[0], $query[1]);
        echo "\n";
    }

}

function input() {
    echo "input: \n";
    $inputLineOne = fopen("php://stdin","r");
    $lineOne = trim(fgets($inputLineOne));
    $lineOneSplit = explode(" ", $lineOne);
    if (count($lineOneSplit) !== 2) {
        echo "incorrect input";
        return;
    }

    $n = $lineOneSplit[0];
    $q = $lineOneSplit[1];
    
    $inputLineTwo = fopen("php://stdin","r");
    $lineTwo = trim(fgets($inputLineTwo));
    $lineTwoLen = strlen($lineTwo);
    
    if ($lineTwoLen != $n) {
        echo "incorrect string length";
        return;
    }
    
    $queries = [];
    
    for ($i = 0; $i < $q; $i++) {
        $inputQuery = fopen("php://stdin","r");
        $query = trim(fgets($inputQuery));
        $queryArray = explode(" ", $query);
        if ($queryArray[0] < 0 || $queryArray[0] > $n-1 || $queryArray[1] > $n-1 || $queryArray[0] > $queryArray[1]) {
            echo "incorrect input";
            return;
        }
        array_push($queries, $queryArray);
    }
    echo "result: \n";
    uniquesubString ($lineTwo, $queries);
    return $queries;
}

$queries = input();